.\" Manpage for sbbdep.
.\" Contact harald.achitz@gmail.com to correct errors or typos.
.TH man 1 "14 Juni 2016" "${sbbdep_VERSION}" "sbbdep man page"
.SH NAME
sbbdep \- Slack Build Binary Dependencies
.br
The tool for exploring binary runtime dependencies on Slackware and Slackware based systems


.SH SYNOPSIS
sbbdep [\fBOPTIONS\fR]... [\fIQUERY\fR]

\fIQUERY\fR can be:
.RS 7
A single binary/library file
.br 
A file from /var/adm/packages
.br 
A package DESTDIR of a slackbuild
.br 
Some existing file
.RE

If \fIQUERY\fR is omitted sbbdep runs only the cache synchronisation.
.br
The default behavior is to find dependencies of \fIQUERY\fR.
.br
If \fIQUERY\fR is not a binary file or an installed package but a valid file name
.br
sbbdep will search for it in the package database and reports the package it belongs to.


.SH OPTIONS

.IP  "\fB-c, --cache=[\fIFILENAME\fB]\fR" 
Cache file to use. The cache file must be read/writable.
.br
If this option is omitted \fI$XDG_CACHE_HOME/sbbdep.db\fR is used.

.IP "\fB-f, --file=[\fIFILENAME\fB]\fR"
Write output to file
.B \fIFILENAME\fR
.br
If this option is omitted the output will go to \fIstdout\fR.   

.IP "\fB-s, --short" 
Report package name without version


.IP \fB--nosync 
Skip synchronise the cache. Only useful if cache is up to date.
.br
If the cache is out of date wrong results might be reported. 

.IP \fB--quiet
Suppress status information during sync

.IP \fB--require
This is the default option if not \fB--whoneeds\fR is used.
.br
If this option is explicit given and \fIQUERY\fR is not a binary file 
.br
The search in the package database will be skipped.  

.IP \fB--whoneeds
Show packages that depend on \fIQUERY\fR.
.br
Using this option will show which packages need a certain one. 


.IP \fB--xdl
Explain dynamic linked file, reports detailed information.
.br
Works with require and whoneeds queries.


.IP \fB--bdtree
Prints a hierarchical dependency tree of \fIQUERY\fR.
.br
This can produce a quite long output of for some binaries or libraries.
.br
This can produce a very very big and long output for some package.

.IP \fB--ignore=[\fINAME,LIST\fR]
Takes a comma separated list of package names to ignore in the output.
.br
Either the base name or the full package name can be used. 
.br
\fIaaa_base,cxxlibs\fR might be an example for this option.
.br
This option is valid for dependency queries, useless otherwise.

.IP "\fB-l, --lookup"
Search for \fIQUERY\fR in the package database and skip all other operations.
                             
.IP \fB--admdir=[\fIDIRNAME\fR]
Usually you do not want to use this option.
.br
Use \fIDIRNAME\fR as package database.
.br
The default is /var/adm/packages.

                         

.IP \fB--ldd
You do not want to use this option to obtain dependency information.
.br
Using this option sbbdep will use the ldd command.
.br
This will not only print direct requirements of a given file.
.br
It will also pull in the dependencies of the requirements. 
.br
This option is useless for a \fB--whoneeds\fR query.


.IP "\fB-v,  --version"
Display sbbdep version.

.IP "\fB-h,  --help"
Display this text.



.SH FILES
.I $XDG_CACHE_HOME/sbbdep.db
.RS
The cache sbbdep creates and uses.
.br
The filename can be changed via the \fB-c/--cache\fR option.
.br
Whatever file you use you might want to keep it.



.SH BUGS
No known bugs at the moment.
.br
If you have found a bug or want to place a feature request please use the issue tracer.
.br
https://bitbucket.org/a4z/sbbdep/issues

.SH SEE ALSO
There is a comprehensive documentation online.
.br
https://bitbucket.org/a4z/sbbdep/wiki/Home

.SH AUTHOR
Harald Achitz (harald.achitz@gmail.com)
